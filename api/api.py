from json import load
from os import getenv
from typing import Dict, Optional

from flask import Blueprint, render_template, jsonify
from flask_login import login_required
from flask.views import View
from service.load_data import get_super_set_data


api_bp = Blueprint('api_bp', __name__)

atm_locator: Dict[str, "AtmLocator"] = {}

raw_data = get_super_set_data('https://www.ing.nl/api/locator/atms/')
for _type in raw_data:
    if isinstance(_type, list):
        count = 0
        for each in data:
            count += 1
            atm_locator = AtmLocator(
                id=count,
                address=each["address"],
                distance=each["distance"],
                openingHours=each["openingHours"],
                functionality=each["functionality"],
                type=each["type"],
            )



class AtmLocatorView(View):
    methods = ['GET', 'POST', 'PUT', 'DELETE']
    decorators = [login_required]

    def get(self):
        search = request.get('city')
        if search:
            newDict = dict(filter(lambda elem: elem['address']['city'] == search, atm_locator.items()))
            return newDict, 200
        return atm_locator, 200

    def post(self):
        data = request.data
        count += 1
        AtmLocator(
            id=count,
            address=data["address"],
            distance=data["distance"],
            openingHours=data["openingHours"],
            functionality=data["functionality"],
            type=data["type"],
        )

        return {"msg": "Successfully Created"}, 201

    def put(self, _id):
        data = request.data
        for each in atm_locator:
            if each['id'] == _id:
                atm_locator_data = atm_locator['id']
                atm_locator_data["address"] = data['address']
                atm_locator_data["distance"] = data['distance']
                atm_locator_data["openingHours"] = data['openingHours']
                atm_locator_data["functionality"] = data['functionality']
                atm_locator_data["type"] = data['type']
                break

        return atm_locator_data, 202


    def delete(self, id):
        for each in atm_locator:
            if each['id'] == _id:
                del atm_locator
                break
        return {"msg": "Successfully Deleted"}, 204


api_bp.add_url_rule('/atm/locator/', view_func=AtmLocatorView.as_view('atm_locator_view'))
api_bp.add_url_rule('/atm/locator/<int:id>', view_func=AtmLocatorView.as_view('get_atm_locator_view'))
