from json import load
from os import getenv
from typing import Dict, Optional

from flask import Blueprint, render_template
from flask.views import View

from flask_login import (
    LoginManager,
    UserMixin,
    current_user,
    login_required,
    login_user,
    logout_user,
)
from data.user import users_data


login_bp = Blueprint('login_bp', __name__)

login_manager = LoginManager(login_bp)

users: Dict[str, "User"] = {}


class User(UserMixin):
    def __init__(self, id: str, username: str, password: str):
        self.id = id
        self.username = username
        self.password = password

    @staticmethod
    def get(user_id: str) -> Optional["User"]:
        return users.get(user_id)

    def __str__(self) -> str:
        return f"<Id: {self.id}, Username: {self.username}>"

    def __repr__(self) -> str:
        return self.__str__()


for key, val in users_data.items():
    users[key] = User(
        id=key,
        username=val["username"],
        password=val["password"],
    )


@login_manager.user_loader
def load_user(user_id: str) -> Optional[User]:
    return User.get(user_id)


class IndexView(View):
    methods = ['GET']

    def get(self):
        username = "anonymous"
        if current_user.is_authenticated:  # type: ignore
            username = current_user.username  # type: ignore
        return f"""
            <h1>Hi {username}</h1>
            <h3>Welcome to Flask Login without ORM!</h3>
        """


class LoginView(View):
    methods = ['POST']

    def get(self):
        data = request.data
        user = User.get(username=data.get('username'))
        print(user)
        if user and user.password == data.get('password'):
            login_user(user)
            return redirect(url_for("index"))
        return {"msg": "Invalid user id or password"}, 400


class LogoutView(View):
    methods = ['GET']
    decorators = [login_required]

    def get(self):
        logout_user()
        return redirect(url_for("index"))


login_bp.add_url_rule('/', view_func=IndexView.as_view('index_view'))
login_bp.add_url_rule('/login/', view_func=LoginView.as_view('login_view'))
login_bp.add_url_rule('/logout/', view_func=LogoutView.as_view('logout_view'))
