# This App is for repersenting the atm locator API's

* Need to install pipenv on your system (Ref: https://pipenv.pypa.io/en/latest/)
* After installing the pipenv run the follow the steps

** Run Command:
* pipenv shell
* pipenv install -d
* python run.py

** For test Command:
* python -m pytest


** For server setup:

<!-- This is the normal setup -->
* Create a instance on EC2 or DigitalOcean
* Use ubuntu server
* Install all required setup like: postgres, python, etc
* Install pipenv or create virtualenv
* Clone git repo
* If Vistual env: run pip install -r requirements.txt
* Else: pipenv install -d

* Setup gunicon
* Setup nginx
* Setup Supervisior

<!-- Setup using the docker -->
* Create a instance on EC2 or DigitalOcean
* Use ubuntu server
* Install all required setup like: postgres, python, docker, etc
* Clone git repo
* Run docker commands:
* docker-compose up --build -d
