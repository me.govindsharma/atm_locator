from flask import Flask

from api.login import login_bp
from api.api import api_bp


app = Flask(__name__)

app.register_blueprint(login_bp)
app.register_blueprint(api_bp)
