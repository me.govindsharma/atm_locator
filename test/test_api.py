import unittest
from test import client


class testApi(unittest.TestCase):
    """docstring for testApi"""
    view_post = {
        "id": 1,
        "address": {
            "street": "Dracht",
            "housenumber": "60",
            "postalcode": "8442 BS",
            "city": "Heerenveen",
            "geoLocation": {
                "lat": "52.958848",
                "lng": "5.924158"
            }
        },
        "distance": 0,
        "openingHours": [{
            "dayOfWeek": 2,
            "hours": [{
                "hourFrom": "13:00",
                "hourTo": "18:00"
            }]
        }, {
            "dayOfWeek": 3,
            "hours": [{
                "hourFrom": "09:30",
                "hourTo": "18:00"
            }]
        }, {
            "dayOfWeek": 4,
            "hours": [{
                "hourFrom": "09:30",
                "hourTo": "18:00"
            }]
        }, {
            "dayOfWeek": 5,
            "hours": [{
                "hourFrom": "09:30",
                "hourTo": "21:00"
            }]
        }, {
            "dayOfWeek": 6,
            "hours": [{
                "hourFrom": "09:30",
                "hourTo": "18:00"
            }]
        }, {
            "dayOfWeek": 7,
            "hours": [{
                "hourFrom": "09:30",
                "hourTo": "17:00"
            }]
        }, {
            "dayOfWeek": 1,
            "hours": []
        }],
        "functionality": "Geld storten en opnemen",
        "type": "GELDMAAT"
    }
    
    def test_atm_locator_create(self):
        res = client.post("/atm/locator/", data=view_post)
        assert res.status == 201


    def test_atm_locator_get(self):
        res = client.get("/atm/locator/")
        assert res.status == 200

    def test_atm_locator_put(self):
        view_post["distance"] = 1
        res = client.put("/atm/locator/1", data=view_post)
        assert res.status == 202

    def test_atm_locator_delete(self):
        view_post["distance"] = 1
        res = client.delete("/atm/locator/1")
        assert res.status == 204
